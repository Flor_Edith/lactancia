<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePruebasDonadoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pruebas_donadora', function (Blueprint $table) {
            $table->increments('id_pruebas');
            $table->integer('id_madre')->unsigned();
            $table->integer('tipo')->unsigned();
            $table->foreign('id_madre')->references('id_madre')->on('madre_donadora')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pruebas_donadora');
    }
}
