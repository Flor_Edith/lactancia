    @extends('principal')
    @section('contenido')
        <template v-if="menu==0">
            <inicio></iniciot>
        </template>

        <template v-if="menu==1">
           <madre-donadora></madre-donadora>
        </template>

        <template v-if="menu==2">
            <pruebas-donadora></pruebas-donadora>
        </template>

        <template v-if="menu==3">
            <bebe></bebe>
        </template>

        <template v-if="menu==4">
            <leche></leche>
        </template>

        <template v-if="menu==5">
            <analisis-leche></analisis-leche>
        </template>

        <template v-if="menu==6">
            <pasteurizacion></pasteurizacion>
        </template>

        <template v-if="menu==7">
            <sucedaneo></sucedaneo>
        </template>

        <template v-if="menu==8">
            <dosificacion></dosificacion>
        </template>

        <template v-if="menu==9">
            <reporte1></reporte1>
        </template>

        <template v-if="menu==10">
            <reporte1></reporte1>
        </template>

        <template v-if="menu==11">
            <h1>Contenido del menú 11</h1>
        </template>

        <template v-if="menu==12">
            <h1>Contenido del menú 12</h1>
        </template>
        
    @endsection