
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('inicio', require('./components/Inicio.vue'));
Vue.component('madre-donadora', require('./components/MadreDonadora.vue'));
Vue.component('pruebas-donadora', require('./components/Pruebas.vue'));
Vue.component('bebe', require('./components/Bebe.vue'));
Vue.component('leche', require('./components/Leche.vue'));
Vue.component('analisis-leche', require('./components/AnalisisLeche.vue'));
Vue.component('pasteurizacion', require('./components/Pasteurizacion.vue'));
Vue.component('sucedaneo', require('./components/Sucedaneo.vue'));
Vue.component('dosificacion', require('./components/Dosificacion.vue'));
Vue.component('reporte1', require('./components/Reporte1.vue'));

const app = new Vue({
    el: '#app',
    data :{
        menu : 0
    }
});
