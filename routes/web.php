<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('contenido/contenido');
});

Route::get('/madreDonadora','MadreDonadoraController@index');
Route::post('/madreDonadora/registrar','MadreDonadoraController@store');
Route::post('/madreDonadora/actualizar','MadreDonadoraController@update');

Route::get('/bebe','BebeController@index');
Route::post('/bebe/registrar','BebeController@store');
Route::put('/bebe/actualizar','BebeController@update');
Route::put('/bebe/desactivar','BebeController@desactivar');
Route::put('/bebe/activar','BebeController@activar');