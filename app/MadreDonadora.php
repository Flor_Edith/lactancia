<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MadreDonadora extends Model
{
    //
    protected $table = 'madre_donadora';
    protected $primaryKey ='id_madre';
    protected $fillable =['nombre_donadora','created_at','updated_at'];
}
