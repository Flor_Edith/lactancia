<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bebe extends Model{

    protected $table='bebe';
    protected $primaryKey='id_bebe';
    protected $fillname = ['nombre_madre','fecha_nacimiento','condicion'];
    public $timestamps = false;
}
