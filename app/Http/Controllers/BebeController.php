<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bebe;

class BebeController extends Controller
{
    public function index()
    {
        $bebes= Bebe::all();
        return $bebes;
    }

    public function store(Request $request)
    {
        $bebe = new Bebe(); 
        $bebe->nombre_madre= $request->nombre_madre;
        $bebe->fecha_nacimiento= $request->fecha_nacimiento;
        $bebe->save();
    }

    public function update(Request $request)
    {
        $bebe = Bebe::findOrFail($request->id_bebe); 
        $bebe->nombre_madre= $request->nombre_madre;
        $bebe->fecha_nacimiento= $request->fecha_nacimiento;
        $bebe->save();
    }

    public function desactivar(Request $request)
    {
        $bebe = Bebe::findOrFail($request->id_bebe);
        $bebe->condicion = 0;
        $bebe->save();
    }

    public function activar(Request $request)
    {
        $bebe = Bebe::findOrFail($request->id_bebe);
        $bebe->condicion = 1;
        $bebe->save();
    }
}
