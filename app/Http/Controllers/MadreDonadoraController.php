<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MadreDonadora;


class MadreDonadoraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $madreDonadora = MadreDonadora::all();
        return $madreDonadora;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $madreDonadora = new MadreDonadora();
        $madreDonadora->nombre_donadora = $request->nombre_donadora;
        $madreDonadora->created_at = $request->updated_at;
        $madreDonadora->updated_at = $request->updated_at;
        $madreDonadora->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_madre)
    {
        $madreDonadora = MadreDonadora::findOrFail($request->id_madre);
        $madreDonadora->nombre_donadora = $request->nombre_donadora;
        $madreDonadora->updated_at = $request->updated_at;
        $madreDonadora->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function desactivar(Request $request)
    {
        //
    }
    public function activar(Request $request)
    {
        //
    }
}
